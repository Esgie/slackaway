#Slack Away Message
Formatted like an away message, Slack-Away changes you last name in Slack to give other users more context around what you are currently doing.

#Requirements
Requires Yosemite or later
